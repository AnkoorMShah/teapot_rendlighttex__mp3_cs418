/******************************************************************************
Ankoor M Shah
amshah4
cs 418 - MP3
******************************************************************************/



#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

#include <GL/glew.h>
#include <GL/glut.h>
#include <SOIL/SOIL.h>



//STRUCTS
typedef struct _Vector3f
{
	GLfloat component[3];

} Vector3f;

typedef struct _Vertex3f
{
	GLfloat coordinate[3];
	GLfloat texture[2];
	Vector3f n;

} Vertex3f;

typedef struct _Triangle3f
{
	size_t vertex[3];
	Vector3f n;

} Triangle3f;



//FUNCTION PROTOTYPES
void init(int argc, char** argv);
void initVerticies();
void loadGLTextures();

void display(void);
void reshape (int w, int h);
void keyboard(unsigned char key, int x, int y);
void animation(int dummy);
void specialKey(int key, int x, int y);



//GLOBALS
int FPS = 60;
const int axisSize = 1;	//pertains to desired max draw distance (+-) but limited by
int colorFilled=1;	//1==yes; else=>no
int simpleTexture=0;	//0==simple, 1==spheremapped, 2==both
int rectangularTM=false;	//true==rec, false==spherical

GLfloat thetaxz=0;
GLfloat thetayz=0;
GLfloat thetaWorldxz=0;
GLfloat thetaWorldyz=0;
GLfloat thetaLight=0;

//lookat eye
const GLfloat ex	= 0.0;
const GLfloat ey	= 0.2;
const GLfloat ez	= 0.5;


//texture stuff
GLuint texture[2];	//0==simple, 1==spheremapped


//vertecies
const size_t numPoints=1202;
const size_t numTriangles=2256;
const size_t numComponents=3;
const size_t numCoordinates=3;
const size_t numVerticies=3;
Vertex3f point[1202];
Triangle3f triangle[2256];



/******************************************************************************
Handle drawing. All functions changing what is drawn will only change variables
and perspectives used by this function.

@param - none;
@return - Current frame of dancing drawn;
******************************************************************************/
void display(void)
{
	GLfloat grey[] = {0.2, 0.2, 0.2, 1.0};
	GLfloat ambGrey[] = {0.01, 0.01, 0.01, 1.0};
	GLfloat lpos[] = {ex, ey*1.2, ez, 0.0};

	if(colorFilled==1)
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	else
	{
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

	//chose texture
	if(simpleTexture==0)
	{
		glDisable(GL_TEXTURE_2D);
		glDisable(GL_TEXTURE_GEN_S);
		glDisable(GL_TEXTURE_GEN_T);
		glActiveTexture(GL_TEXTURE0);
		glEnable(GL_TEXTURE_2D);		
	}
	else if(simpleTexture==1)
	{
		glDisable(GL_TEXTURE_2D);
		glActiveTexture(GL_TEXTURE1);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_TEXTURE_GEN_S);
		glEnable(GL_TEXTURE_GEN_T);
	}
	else
	{
		glActiveTexture(GL_TEXTURE0);
		glEnable(GL_TEXTURE_2D);

		glActiveTexture(GL_TEXTURE1);
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_TEXTURE_GEN_S);
		glEnable(GL_TEXTURE_GEN_T);
	}


	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	// reset OpenGL transformation matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();											//clear the matrix
	gluLookAt (ex, ey, ez, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);		//eye does not move

	
	//movement of world is done by rotations, since focus object (teapot) stays at center
	glPushMatrix();
		//rotate world including light
		glRotatef(thetaWorldxz, 0.0, 1.0, 0.0);
		glRotatef(thetaWorldyz, 1.0, 0.0, 0.0);

		glPushMatrix();
			glRotatef(thetaLight, 0.0, 0.0, 1.0);				//rotating light
			thetaLight=(thetaLight+0.1);

			glLightfv(GL_LIGHT0, GL_POSITION, lpos);
			glLightfv(GL_LIGHT0, GL_AMBIENT, ambGrey);
			glLightfv(GL_LIGHT0, GL_DIFFUSE, grey);
			glLightfv(GL_LIGHT0, GL_SPECULAR, grey);
		glPopMatrix();

		
		glPushMatrix();
			//rotate pot independent of light with wasd
			glRotatef(thetaxz, 0.0, 1.0, 0.0);
			glPushMatrix();
				glRotatef(thetayz, 1.0, 0.0, 0.0);

				//glutSolidTeapot(0.2f); //Draw the teapot 
				glScalef(0.1, 0.1, 0.1);
				glTranslatef(0.0, -1.0, 0.0);
				
				for(int i=0; i<numTriangles; i++)
				{
					GLfloat nx[3];
					GLfloat ny[3];
					GLfloat nz[3];
					GLfloat vx[3];
					GLfloat vy[3];
					GLfloat vz[3];
					GLfloat ts[3];
					GLfloat tt[3];


					for(int j=0; j<numVerticies; j++)
					{
						//vertex and normal XYZs, for increased readability
						size_t p=triangle[i].vertex[j];
						nx[j]=point[p].n.component[0];
						ny[j]=point[p].n.component[1];
						nz[j]=point[p].n.component[2];
						vx[j]=point[p].coordinate[0];
						vy[j]=point[p].coordinate[1];
						vz[j]=point[p].coordinate[2];

						if(rectangularTM)
						{
							ts[j]=point[p].coordinate[0];
							tt[j]=point[p].coordinate[1];
						}
						else
						{
							ts[j]=point[p].texture[0];
							tt[j]=point[p].texture[1];
						}
					}

					glBegin(GL_TRIANGLES);
						glNormal3f(nx[0], ny[0], nz[0]);
						glTexCoord2f(ts[0], tt[0]);
						glVertex3f(vx[0], vy[0], vz[0]);

						glNormal3f(nx[1], ny[1], nz[1]);
						glTexCoord2f(ts[1], tt[1]);
						glVertex3f(vx[1], vy[1], vz[1]);

						glNormal3f(nx[2], ny[2], nz[2]);
						glTexCoord2f(ts[2], tt[2]);
						glVertex3f(vx[2], vy[2], vz[2]);
					glEnd();
				}
			glPopMatrix();
		glPopMatrix();
	glPopMatrix();


	glutSwapBuffers();
	glFlush();
	glutPostRedisplay();
	return;
}



/******************************************************************************
MAIN
******************************************************************************/
int main(int argc, char** argv)
{
	init(argc, argv);


	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(specialKey);

	glutTimerFunc(100, animation, 1);


	glutMainLoop();
	return 0;
}



/******************************************************************************
Handles animation by adjusting variables every FPS times a second

@param - dummy input required by callback
@return - display will draw adjusted figure;
******************************************************************************/
void animation(int dummy)
{
	glutPostRedisplay();
	glutTimerFunc(1000/FPS, animation, dummy); // restart timer again
	return;
}


/******************************************************************************
Establish background color. Initialize verticies and triangles based on
input file.

@param - none;
@return - default background color established;
******************************************************************************/
void init(int argc, char** argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
	//center screen and give it default size.
	int w=960;
	int h=540;
	glutInitWindowSize(w, h);
	glutInitWindowPosition(		glutGet(GLUT_SCREEN_WIDTH)/2-w/2,
								glutGet(GLUT_SCREEN_HEIGHT)/2-h/2);
	glutCreateWindow(argv[0]);
	glewInit();


	initVerticies();
	loadGLTextures();
	
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,	GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,	GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, 		GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,		GL_REPEAT);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, texture[1]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,	GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,	GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, 		GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T,		GL_CLAMP);
	glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);
	glTexEnvi(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
	

	glShadeModel(GL_SMOOTH); 
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glClearColor (0.0, 0.0, 0.0, 0.0);
	glEnable(GL_DEPTH_TEST);
}


/******************************************************************************
Handles loading the .bmp based on sources mentioned in the README.

@param - none
@return -	texture is initialized; will [gracefully] crash program on
			failure, (design choice).
******************************************************************************/
void loadGLTextures()
{
	//SOIL STUFFS
	texture[0] = SOIL_load_OGL_texture(	"simple.bmp",
										SOIL_LOAD_AUTO,
										SOIL_CREATE_NEW_ID,
										SOIL_FLAG_MIPMAPS | 
										SOIL_FLAG_INVERT_Y | 
										SOIL_FLAG_NTSC_SAFE_RGB | 
										SOIL_FLAG_COMPRESS_TO_DXT);
	texture[1] = SOIL_load_OGL_texture(	"env.hdr",
										SOIL_LOAD_AUTO,
										SOIL_CREATE_NEW_ID,
										SOIL_FLAG_MIPMAPS | 
										SOIL_FLAG_INVERT_Y | 
										SOIL_FLAG_NTSC_SAFE_RGB | 
										SOIL_FLAG_COMPRESS_TO_DXT);

	if(texture[0]==0)
	{
		cerr << "simple.bmp DNE";
		exit(2);
	}
	if(texture[1]==0)
	{
		cerr << "env.hdr DNE";
		exit(3);
	}


	return;
}


/******************************************************************************
Initialize verticies and triangles based on
input file.

@param - none;
@return -	vertex and triangle arrays initialized. [Gracefully] Crashes
			program if file	doesn't exist, (design choice).
******************************************************************************/
void initVerticies()
{
	string line;
	ifstream teapotObj("teapot_0.obj");
	size_t ptsi=0;
	size_t ti=0;
	GLfloat ymax=0;


	if(teapotObj.is_open())
	{
		while(getline(teapotObj, line))
		{
			if(line[0]=='v')
			{
				//store vertex at appropriate index
				size_t st=2;
				for(int i=0; i<numCoordinates; i++)
				{
					size_t fin=line.find(' ', st);
					point[ptsi].coordinate[i]=atof( (line.substr(st, fin-st)).c_str() );
					
					st=fin+1;
				}
				
				for(int i=0; i<numComponents; i++)
				{
					point[ptsi].n.component[i]=0;
				}


				ptsi++;
			}
			else if(line[0] == 'f')
			{
				//store indecies of vertecies for each triabgle
				size_t st=3;
				for(int i=0; i<numVerticies; i++)
				{
					size_t fin=line.find(' ', st);
					//file starts with vertex index 1 randomly, therefore must subtract 1
					triangle[ti].vertex[i]=atoi( (line.substr(st, fin-st)).c_str() ) -1;


					st=fin+1;
				}


				//calculate normal and add to all 3 vertices
				//http://www.opengl.org/wiki/Calculating_a_Surface_Normal#Perl_version_for_a_triangle:
				Vector3f U;
				Vector3f W;
				//the following 3 are simply to aid readability
				size_t p0=triangle[ti].vertex[0];
				size_t p1=triangle[ti].vertex[1];
				size_t p2=triangle[ti].vertex[2];

				for(int i=0; i<numComponents; i++)
				{
					U.component[i]=	point[p1].coordinate[i] - point[p0].coordinate[i];
					W.component[i]=	point[p2].coordinate[i] - point[p0].coordinate[i];
				}

				triangle[ti].n.component[0]=	(U.component[1]*W.component[2]) -
												(U.component[2]*W.component[1]);
				triangle[ti].n.component[1]=	(U.component[2]*W.component[0]) -
												(U.component[0]*W.component[2]);
				triangle[ti].n.component[2]=	(U.component[0]*W.component[1]) -
												(U.component[1]*W.component[0]);

				//unitize
				GLfloat magn=0;
				for(int i=0; i<numComponents; i++)
				{
					magn+=triangle[ti].n.component[i]*triangle[ti].n.component[i];
				}
				magn=sqrt(magn);
				for(int i=0; i<numComponents; i++)
				{
					triangle[ti].n.component[i]/=magn;
				}

				//accumilate face normals to all vector normals
				for(int i=0; i<numComponents; i++)
				{
					point[p0].n.component[i]+=triangle[ti].n.component[i];
					point[p1].n.component[i]+=triangle[ti].n.component[i];
					point[p2].n.component[i]+=triangle[ti].n.component[i];
				}


				ti++;
			}
			//else ignore
		}


		teapotObj.close();
	}
	else
	{
		cerr << "\nteapot_0.obj DNE\n";
		exit(1);
	}


	//normalize vertecies
	for(int i=0; i<numPoints; i++)
	{
		GLfloat magn=0;
		for(int j=0; j<numComponents; j++)
		{
			magn+=point[i].n.component[j]*point[i].n.component[j];
		}
		magn=sqrt(magn);
		for(int j=0; j<numComponents; j++)
		{
			point[i].n.component[j]/=magn;
		}


		if(point[i].coordinate[1] > ymax)
		{
			ymax=point[i].coordinate[1];
		}
	}


	//texture coordinates using cylindrical coordinates
	for(int i=0; i<numPoints; i++)
	{
		GLfloat theta=atan2(point[i].coordinate[2], point[i].coordinate[0]);
		const GLfloat PI=3.14159;

		point[i].texture[0]=(theta+PI)/(2*PI);
		point[i].texture[1]=point[i].coordinate[1] / ymax;
	}


	return;
}


/******************************************************************************
Changes perspective to ensure display draws scaled to the window.

@param - none;
@return - gluPerspective adjusted;
******************************************************************************/
void reshape (int w, int h)
{
	glViewport (0, 0, (GLsizei) w, (GLsizei) h);
	glMatrixMode (GL_PROJECTION);
	glLoadIdentity ();
	gluPerspective(90.0,1.0,0.01,10.0);
	glMatrixMode (GL_MODELVIEW);


	return;
}


/******************************************************************************
Keyboard interactivity.

@param - keyboard key triggering event;
@return - (My added ones only listed:
	Space	=	Switch between outline and color-filled modes;
	'q'
	'Q'		=	terminate program;

	{w,a,s,d}
	{W,A,S,D}	=	rotate pot

	{UP,DOWN,LEFT,RIGHT}	=	rotate world

	'1'		=	use simple texture mapping
	'2'		=	use environment texture mapping

	default	=	cout entered key
******************************************************************************/
void keyboard(unsigned char key, int x, int y)
{
   switch (key)
   {
		case 'w':
		case 'W':
			thetayz+=3;
			break;
		case 's':
		case 'S':
			thetayz-=3;
			break;
		case 'a':
		case 'A':
			thetaxz+=3;
			break;
		case 'd':
		case 'D':
			thetaxz-=3;
			break;

		case ' ':
			colorFilled =! colorFilled;
			break;
		case '1':
			simpleTexture = 0;
			break;
		case '2':
			rectangularTM = true;
			break;
		case '3':
			rectangularTM = false;
			break;
		case '4':
			simpleTexture = 1;
			break;
		case '5':
			simpleTexture = 2;
			break;
		default:
			cout << key << endl;
			break;
   }


   return;
}

void specialKey(int key, int x, int y)
{
	switch (key)
   {
		case GLUT_KEY_UP:
			thetaWorldyz+=3;
			break;
		case GLUT_KEY_DOWN:
			thetaWorldyz-=3;
			break;
		case GLUT_KEY_LEFT:
			thetaWorldxz+=3;
			break;
		case GLUT_KEY_RIGHT:
			thetaWorldxz-=3;
			break;
		default:
			cout << key << endl;
			break;
   }


	return;
}


