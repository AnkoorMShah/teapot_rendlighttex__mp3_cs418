Move pot with wasd, independent of light.
Move platfortm, (pot and lightsource), with arrow keys.

Pressing 1 separate environment and simple textures. Pressing 5 combines them.
Pressing 4 will switch to environment, if not already combined. Pressing 1 will reset to normal textures.
Pressing 2 and 3 switches between rectangular texture coordinate mapping (2), and sphericical (3).

There is a texture glitch with spherical simple texture mapping: A portion by the handle is not rendered correctly. I tried using offsets for s, and adjusting several other variables around. Could not find solution.

In executable folder, run amshah4_mp3.exe. Other files, including .dll, images, and .obj are all needed.



SOURCES (used lightly for small details):
________________________________
http://www.opengl.org/
http://www.glprogramming.com/red/chapter05.html
http://nehe.gamedev.net/
http://www.cplusplus.com/



